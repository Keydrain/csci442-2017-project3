package vision.robot.csci442_2017_project3;

/*
 * Clint Cooper
 *
 * Note: I tried to get the eye cascade working and tried several different cascades.
 * None of them were picking up my eyes correctly. Usually it said my whole face was an eye.
 * Went with a minor learning approach similar to:
 *      http://romanhosek.cz/android-eye-detection-and-tracking-with-opencv/
 * Doesn't work great, but usually finds both eyes and tracks them in easy conditions.
 *
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.opencv.imgproc.Imgproc.TM_SQDIFF;
import static org.opencv.imgproc.Imgproc.TM_SQDIFF_NORMED;

public class MainActivity extends Activity implements CvCameraViewListener2 {

    private static final String TAG = "CSCI422-2017-Project3";
    private static final Scalar FACE_RECT_COLOR = new Scalar(0, 255, 0, 255);
    private static final Scalar EYE_RECT_COLOR_LEFT = new Scalar(0, 0, 255, 255);
    private static final Scalar EYE_RECT_COLOR_RIGHT = new Scalar(255, 0, 0, 255);
    int method = 1;
    double xCenter = -1;
    double yCenter = -1;
    private int learn_frames = 0;
    private Mat teplateR;
    private Mat teplateL;
    private Mat imgRGBA;
    private Mat imgGray;
    private float mRelativeFaceSize = 0.3f;
    private int mAbsoluteFaceSize = 0;
    private float mRelativeEyeSize = 0.3f;
    private int mAbsoluteEyeSize = 0;

    private CascadeClassifier mJavaDetectorFace;
    private CascadeClassifier mJavaDetectorEye;
    private CascadeClassifier mJavaDetectorEyeGen;
    private CameraBridgeViewBase mOpenCvCameraView;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    File mCascadeFileFace = loadCascade("haarcascade_frontalface_default");
                    mJavaDetectorFace = new CascadeClassifier(mCascadeFileFace.getAbsolutePath());
                    mJavaDetectorFace.load(mCascadeFileFace.getAbsolutePath());
                    if (mJavaDetectorFace.empty()) {
                        Log.e(TAG, "Failed to load face cascade classifier");
                        mJavaDetectorFace = null;
                    } else {
                        Log.i(TAG, "Loaded cascade face classifier from " + mCascadeFileFace.getAbsolutePath());
                    }

                    // Was originally trying to get eyes to match, but never worked quite right.
                    File mCascadeFileEye = loadCascade("haarcascade_lefteye_2splits");
                    mJavaDetectorEye = new CascadeClassifier(mCascadeFileEye.getAbsolutePath());
                    mJavaDetectorEye.load(mCascadeFileEye.getAbsolutePath());
                    if (mJavaDetectorEye.empty()) {
                        Log.e(TAG, "Failed to load eye cascade classifier");
                        mJavaDetectorEye = null;
                    } else {
                        Log.i(TAG, "Loaded cascade eye classifier from " + mCascadeFileEye.getAbsolutePath());
                    }

                    File mCascadeFileEyeGen = loadCascade("haarcascade_eye");
                    mJavaDetectorEyeGen = new CascadeClassifier(mCascadeFileEyeGen.getAbsolutePath());
                    mJavaDetectorEyeGen.load(mCascadeFileEyeGen.getAbsolutePath());
                    if (mJavaDetectorEyeGen.empty()) {
                        Log.e(TAG, "Failed to load eye cascade classifier");
                        mJavaDetectorEyeGen = null;
                    } else {
                        Log.i(TAG, "Loaded cascade eye classifier from " + mCascadeFileEyeGen.getAbsolutePath());
                    }

                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public File loadCascade(String haarfile) {
        File mCascadeFile = null;
        File cascadeDir = null;
        try {
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            mCascadeFile = new File(cascadeDir, haarfile + ".xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
        }
        cascadeDir.delete();
        return mCascadeFile;
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.activity_main);
        mOpenCvCameraView.setCameraIndex(1);
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        imgGray = new Mat();
        imgRGBA = new Mat();
    }

    public void onCameraViewStopped() {
        imgGray.release();
        imgRGBA.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        imgRGBA = inputFrame.rgba();
        imgGray = inputFrame.gray();

        if (mAbsoluteFaceSize == 0) {
            int height = imgGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }
        MatOfRect faces = new MatOfRect();
        mJavaDetectorFace.detectMultiScale(imgGray, faces, 1.1, 4, 2, new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
        Rect[] facesArray = faces.toArray();

        for (Rect aFace : facesArray) {
            Imgproc.rectangle(imgRGBA, aFace.tl(), aFace.br(), FACE_RECT_COLOR, 3);
            xCenter = (aFace.x + aFace.width + aFace.x) / 2;
            yCenter = (aFace.y + aFace.y + aFace.height) / 2;

            //Point center = new Point(xCenter, yCenter);
            // Imgproc.circle(imgRGBA, center, 10, new Scalar(255, 255, 255, 255), 3);

            // We need the area for each eye
            Rect eyearea_right = new Rect(
                    aFace.x + aFace.width / 16,
                    (int) (aFace.y + (aFace.height / 4.5)),
                    (aFace.width - 2 * aFace.width / 16) / 2,
                    (int) (aFace.height / 3.0));
            Rect eyearea_left = new Rect(
                    aFace.x + aFace.width / 16 + (aFace.width - 2 * aFace.width / 16) / 2,
                    (int) (aFace.y + (aFace.height / 4.5)),
                    (aFace.width - 2 * aFace.width / 16) / 2,
                    (int) (aFace.height / 3.0));

            //Imgproc.rectangle(imgRGBA, eyearea_left.tl(), eyearea_left.br(), EYE_RECT_COLOR_LEFT, 2);
            //Imgproc.rectangle(imgRGBA, eyearea_right.tl(), eyearea_right.br(), EYE_RECT_COLOR_RIGHT, 2);

            // Try sampling for a good perspective
            if (learn_frames < 200) {
                teplateR = get_template(eyearea_right, 24);
                teplateL = get_template(eyearea_left, 24);
                learn_frames++;
            } else {
                match_eye(eyearea_right, teplateR, method);
                match_eye(eyearea_left, teplateL, method);
            }

        }

        return imgRGBA;
    }

    private Mat get_template(Rect area, int size) {
        Mat template = new Mat();
        Mat mROI = imgGray.submat(area);
        Point iris = new Point();
        Rect eye_template = new Rect();

        double xcent = (area.x + area.width + area.x) / 2;
        double ycent = (area.y + area.y + area.height) / 2;

        Rect eye_only_rectangle = new Rect(
                (int) (area.tl().x + area.width * 0.1 + xcent) / 2,
                (int) (area.tl().y + area.height * 0.35 + ycent) / 2,
                (int) (area.width * 0.4),
                (int) (area.height * 0.4));

        Imgproc.rectangle(imgRGBA, eye_only_rectangle.tl(), eye_only_rectangle.br(), new Scalar(255, 255, 255, 255), 2);

        mROI = imgGray.submat(eye_only_rectangle);
        Mat vyrez = imgRGBA.submat(eye_only_rectangle);

        Core.MinMaxLocResult mmG = Core.minMaxLoc(mROI);

        Imgproc.circle(vyrez, mmG.minLoc, 2, new Scalar(255, 255, 255, 255), 2);
        iris.x = mmG.minLoc.x + eye_only_rectangle.x;
        iris.y = mmG.minLoc.y + eye_only_rectangle.y;
        eye_template = new Rect((int) iris.x - size / 2, (int) iris.y
                - size / 2, size, size);
        Imgproc.rectangle(imgRGBA, eye_template.tl(), eye_template.br(),
                new Scalar(255, 0, 0, 255), 2);
        template = (imgGray.submat(eye_template)).clone();
        return template;

    }

    private void match_eye(Rect area, Mat mTemplate, int type) {

        Point matchLoc;
        Mat mROI = imgGray.submat(area);
        int result_cols = mROI.cols() - mTemplate.cols() + 1;
        int result_rows = mROI.rows() - mTemplate.rows() + 1;

        Mat mResult = new Mat(result_cols, result_rows, CvType.CV_8U);

        // Two approaches for matching the template. Neither works great.
        switch (type) {
            case TM_SQDIFF:
                Imgproc.matchTemplate(mROI, mTemplate, mResult, TM_SQDIFF);
                break;
            case TM_SQDIFF_NORMED:
                Imgproc.matchTemplate(mROI, mTemplate, mResult, TM_SQDIFF_NORMED);
                break;
        }

        Core.MinMaxLocResult mmres = Core.minMaxLoc(mResult);
        if (type == TM_SQDIFF || type == TM_SQDIFF_NORMED) {
            matchLoc = mmres.minLoc;
        } else {
            matchLoc = mmres.maxLoc;
        }

        Point matchLoc_tx = new Point(matchLoc.x + area.x - 50, matchLoc.y + area.y - 25);
        Point matchLoc_ty = new Point(matchLoc.x + mTemplate.cols() + area.x + 50, matchLoc.y + mTemplate.rows() + area.y + 25);

        Imgproc.rectangle(imgRGBA, matchLoc_tx, matchLoc_ty, new Scalar(255, 255, 0, 255), 5);

    }


}


